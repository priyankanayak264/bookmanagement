﻿using BookManagementSystem.Models;
using BookManagementSystem.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagementSystem.Services
{
    public class BookService:IBookService
    {
        private readonly IBookRepository _bookRepository;
        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public async Task<int> AddBook(Book book)
        {
            return await _bookRepository.AddBook(book);
        }

        public async Task<Book> GetBook(int id)
        {
            return await _bookRepository.GetBook(id);
        }

        public async Task<int> UpdateBookName(Book book)
        {
            return await _bookRepository.UpdateBookName(book);
        }

        public async Task<int> DeleteBook(int id)
        {
            return await _bookRepository.DeleteBook(id);
        }
    }
}
