﻿using BookManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagementSystem.Services
{
    public interface IBookService
    {
        Task<int> AddBook(Book book);
        Task<Book> GetBook(int id);

        Task<int> UpdateBookName(Book book);
        Task<int> DeleteBook(int id);
    }
}
