﻿using BookManagementSystem.Models;
using BookManagementSystem.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpPost("AddBook")]
        public async Task<ActionResult<Book>> AddActor(Book book)
        {
            try
            {
                int bId = await _bookService.AddBook(book);

                return CreatedAtAction("GetBook", new { id = bId }, book);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("GetBook/{id}")]
        public async Task<ActionResult<Book>> GetBook(int id)
        {
            try
            {
                var book = await _bookService.GetBook(id);

                return Ok(book);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut("UpdateBookName")]
        public async Task<IActionResult> UpdateBookName(Book book)
        {

            try
            {
                var books = await _bookService.UpdateBookName(book);

                if (books > 0)
                {
                    return Ok("Successfully Updated");
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpDelete("DeleteBook/{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            try
            {
                int result = await _bookService.DeleteBook(id);

                if (result == 0)
                {
                    return NotFound();
                }
                return Ok("Successfully Deleted Data");
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
