﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagementSystem.CustomExceptions
{
    public class SqlsException : Exception
    {
        public SqlsException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
