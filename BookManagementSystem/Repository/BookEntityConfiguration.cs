﻿using BookManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagementSystem.Repository
{
    public class BookEntityConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> typeBuilder)
        {
            typeBuilder.HasKey(b => b.BookId);
            typeBuilder.Property(b => b.BookId).UseIdentityColumn();

            typeBuilder.Property(b => b.BookName).HasMaxLength(50).IsRequired();

            typeBuilder.Property(b => b.AuthorName).HasMaxLength(50).IsRequired();
        }
    }
}
