﻿using BookManagementSystem.CustomExceptions;
using BookManagementSystem.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagementSystem.Repository
{
    public class BookRepository:IBookRepository
    {
        private readonly BookDbContext _bookDbContext;
        public BookRepository(BookDbContext bookDbContext)
        {
            _bookDbContext = bookDbContext;
        }

        public async Task<int> AddBook(Book book)
        {
            int value = 0;
            try
            {
                _bookDbContext.Book.Add(book);

                value = await _bookDbContext.SaveChangesAsync();
            }
            catch (SqlException ex)
            {
                throw new SqlsException("Sorry Can't connect to Database", ex);
            }
            return value;
        }

        public async Task<Book> GetBook(int id)
        {
            try
            {
                var book = await _bookDbContext.Book.FindAsync(id);

                if (book == null)
                {
                    throw new BookNotFoundException("actor data not there in database");
                }
                return book;
            }
            catch (SqlException ex)
            {
                throw new SqlsException("Sorry can't connect to database", ex);
            }
        }

        public async Task<int> UpdateBookName(Book book)
        {
            try
            {
                _bookDbContext.Entry(book).State = EntityState.Modified;

                var result = await _bookDbContext.SaveChangesAsync();
                return result;
            }
            catch (SqlException ex)
            {
                throw new SqlsException("Sorry can't connect to database", ex);
            }
        }

        public async Task<int> DeleteBook(int id)
        {
            try
            {
                var book = await _bookDbContext.Book.FindAsync(id);

                if (book == null)
                {
                    throw new BookNotFoundException("Data not there in the database");
                }

                _bookDbContext.Book.Remove(book);

                var result = await _bookDbContext.SaveChangesAsync();
                return result;
            }
            catch (SqlException ex)
            {
                throw new SqlsException("Sorry can't connect to database", ex);
            }
        }
    }
}
